# Book Management System Documentation

## Introduction

### Overview
This is a Book Management Application that focuses on the ability to add, update, and delete books from a database. Users can upload pictures and edit information such as Title, Description, and Price.

### Purpose
The Book Management Application allows users to efficiently manage a digital library, providing functionalities for uploading book covers and editing book details. It's designed to be user-friendly for both administrators and visitors.

### Technologies Used

#### Frontend
- **Framework**: React (Vite)
- **State Management and Server Communication**: React Query, Axios
- **Styling**: Tailwind CSS
- **Notifications**: Toastify

#### Backend
- **Runtime Environment**: Node.js
- **Framework**: Express
- **Database**: MySQL
- **Middleware and Utilities**:
  - Cors: To enable cross-origin resource sharing
  - Multer: For handling multipart/form-data for uploading files
  - Path: For working with file and directory paths
  - Nodemon: For automatically restarting the node application when file changes are detected
  - Dotenv: For loading environment variables from a .env file


## Getting Started

### Requirements

- Node.js
- npm
- MySQL

### Installation Steps for the Backend

1. **Cloning the Repository**: git clone __
2. **Installing Dependencies**: Run `npm install` to install the required packages.
3. **Setting up the .env File**: Create a `.env` file in the root directory and specify the necessary environment variables:
4. **Starting the Backend Server**: Run `npm start` to start the server.

### Installation Steps for the Frontend

- **Repository**: Run `npm install` to install the required packages.
- **Starting the Application**: Run `npm run dev` to start the server.

## Backend Documentation

### Configuration

- **.env File**: DB_HOST=your_database_host
DB_USER=your_database_user
DB_PASSWORD=your_database_password
DB_DATABASE=your_database_name
SERVER_PORT=your_preferred_port
UPLOAD_PATH=your_upload_path

### Database Setup

- **MySQL Database**: Open MYSQL Workbench
- **Schema Creation**: Instructions for creating the `books` table schema.

### API Endpoints

- **GET /books**: Fetch all books.
- **POST /books**: Add a new book. Details on required fields for `title`, `desc`, `cover` (file upload), and `price`.
- **DELETE /books/:id**: Delete a book by ID.
- **PUT /books/:id**: Update a book by ID.

### File Uploads

- **Multer**: Explanation of how file uploads are handled using `multer`.

## Frontend Documentation

### Components

- **Home**: Description of the main page functionality.
- **BooksList**: Details on the book listing component.
- **ErrorPage and Loading**: Description of error handling and loading components.

### Hooks

- **useQuery and useMutation**: Explanation of their usage for fetching and deleting books.

### Navigation

- **React Router**: Overview of routing and navigation between components.

## Running the Application

- Instructions on how to run both backend and frontend applications together.

## Contributing

- **Guidelines**: How to contribute to the project.
- **Issue Reporting**: How to report issues or suggest enhancements.

## License

- Information about the project's license.

## Additional Notes

- **Code Snippets**: Include code snippets for clarity.
- **Screenshots**: Include screenshots of the application.
- **Troubleshooting**: Tips for common setup issues.
