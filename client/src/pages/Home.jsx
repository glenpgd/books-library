import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import BooksList from '../components/BooksList';
import ErrorPage from './error/ErrorPage';
import Loading from '../components/Loading';
import { getBooks, deleteBooks } from '../utils/API';

function Home() {
  //useQuery
  const {
    isPending,
    isError,
    data: books,
  } = useQuery({ queryKey: ['books'], queryFn: getBooks });

  // useQueryClient
  const queryClient = useQueryClient();

  //useMutation
  const deleteBook = useMutation({
    mutationFn: (id) => deleteBooks(id),
    onSuccess: () => queryClient.invalidateQueries(['books']),
  });

  //FUNCTION
  const handleDelete = async (id) => {
    try {
      const res = await deleteBook.mutateAsync(id);
      console.log('result', res);
      toast.error('Book deleted');
    } catch (error) {
      console.log(error);
    }
  };

  // RETURN
  if (isError) return <ErrorPage />;
  if (isPending) return <Loading />;
  return (
    <div className='bg-gray-50 min-h-screen flex flex-col'>
      <div className='container mx-auto px-4 sm:px-6 lg:px-8 py-8'>
        <h1 className='text-4xl font-semibold text-indigo-700 mb-8'>
          Books Library
        </h1>

        <BooksList books={books} handleDelete={handleDelete} />
        <Link to='Add'>
          <button
            type='button'
            className='mt-8 py-2 px-4 bg-indigo-600 hover:bg-indigo-700 focus:ring-indigo-500 focus:ring-offset-indigo-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg'
          >
            Add New Book
          </button>
        </Link>
      </div>
    </div>
  );
}

export default Home;
