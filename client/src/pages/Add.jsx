import axios from 'axios';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import SubmitForm from '../components/SubmitForm';
let dbURL = 'http://localhost:8800/books';

function Add() {
  // useState
  const [book, setBook] = useState({
    title: '',
    desc: '',
    price: null,
  });
  const [file, setFile] = useState(null);

  //useNavigate
  const navigate = useNavigate();

  // FUNCTIONs
  const handleFile = (e) => setFile(e.target.files[0]);
  const handleChange = (e) => {
    setBook((prev) => ({ ...prev, [e.target.name]: e.target.value }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const formData = new FormData();
    formData.append('cover', file);
    // Append the rest of the book details to formData
    formData.append('title', book.title);
    formData.append('desc', book.desc);
    formData.append('price', book.price);

    try {
      await axios.post(dbURL, formData);
      navigate('/');
      toast.success('Book added');
    } catch (error) {
      console.log(error);
    }
  };

  // RETURN
  return (
    <>
      <SubmitForm
        text='Add a Book'
        handleChange={handleChange}
        handleSubmit={handleSubmit}
        handleFile={handleFile}
      />
    </>
  );
}

export default Add;
