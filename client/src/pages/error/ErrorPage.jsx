function ErrorPage() {
  return (
    <div className='min-h-screen flex flex-col items-center justify-center bg-gray-100 px-4 sm:px-6 lg:px-8'>
      <div className='max-w-md w-full text-center'>
        <h2 className='mt-6 text-3xl font-extrabold text-gray-900'>Oops!</h2>
        <p className='mt-2 text-base text-gray-600'>
          Something went wrong. Were unable to process your request at the
          moment.
        </p>
        <p className='mt-4'>
          <a
            href='/'
            className='font-medium text-indigo-600 hover:text-indigo-500'
          >
            Go back home
          </a>
        </p>
        <p className='mt-2'>
          <a
            href='javascript:history.back()'
            className='font-medium text-indigo-600 hover:text-indigo-500'
          >
            Go back
          </a>
        </p>
        <div className='mt-6'>
          <img
            src='https://via.placeholder.com/400x300'
            alt='Error Illustration'
            className='mx-auto'
          />
        </div>
      </div>
    </div>
  );
}
export default ErrorPage;
