import axios from 'axios';
import { useState } from 'react';
import { useLocation, redirect } from 'react-router-dom';
import { toast } from 'react-toastify';
import SubmitForm from '../components/SubmitForm';
let dbURL = 'http://localhost:8800/books';

function Update() {
  // useState
  const [book, setBook] = useState({
    title: '',
    desc: '',
    cover: '',
    price: null,
  });

  //useLocation
  const location = useLocation();

  //variables
  const bookId = location.pathname.split('/').pop();

  // FUNCTIONS
  const handleChange = (e) => {
    setBook((prev) => ({ ...prev, [e.target.name]: e.target.value }));
  };
  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      await axios.put(`${dbURL}/${bookId}`, book);
      redirect('/');
      toast.success('successfully updated');
    } catch (error) {
      console.log(error);
    }
  };

  // RETURN
  return (
    <>
      <SubmitForm
        text='Update a Book'
        handleChange={handleChange}
        handleSubmit={handleSubmit}
      />
    </>
  );
}

export default Update;
