import { Outlet } from 'react-router-dom';
import Navbar from '../../components/Navbar';

function AllPagesLayout() {
  return (
    <main>
      <Navbar />
      <Outlet />
    </main>
  );
}
export default AllPagesLayout;
