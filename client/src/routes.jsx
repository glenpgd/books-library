import { createBrowserRouter } from 'react-router-dom';
import {
  Add,
  Home,
  Books,
  Update,
  AllPagesLayout,
  ErrorPage,
} from '../src/pages/exports/index.js';

export const router = createBrowserRouter([
  {
    path: '/',
    element: <AllPagesLayout />,
    errorElement: <ErrorPage />,
    children: [
      { index: true, element: <Home /> },
      { path: 'books/:id', element: <Books /> },
      { path: 'add', element: <Add /> },
      { path: 'update/:id', element: <Update /> },
    ],
  },
]);
