// api.js
import axios from 'axios';

export let dbURL = 'http://localhost:8800/books';

export const getBooks = async () => {
  const { data } = await axios.get(dbURL);
  console.log(data);
  return data;
};

export const deleteBooks = async (id) => {
  const { data } = await axios.delete(`${dbURL}/${id}`);
  return data;
};
