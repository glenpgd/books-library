import axios from 'axios';
import { useState } from 'react';
import { getBooks } from '../utils/API';
import { useEffect } from 'react';

function FileUpload() {
  const [file, setFile] = useState();

  // FUNCTIONs
  const handleFile = (e) => {
    setFile(e.target.files[0]);
  };

  useEffect(() => {
    const res = axios.get('http://localhost:8800/');
    console.log(res);
  }, []);

  const handleUpload = async () => {
    const formData = new FormData();

    formData.append('cover', file);
    console.log('Upload route accessed');

    try {
      console.log('Upload route accessed');
      const response = await axios.post(
        'http://localhost:8800/upload',
        formData
      );
      console.log(response);
    } catch (error) {
      console.log(error);
    }
  };

  //RETURN
  return (
    <div>
      <label htmlFor='cover'>Cover URL</label>
      <input type='file' name='cover' onChange={handleFile} />
      <button onClick={handleUpload}>Upload</button>
    </div>
  );
}
export default FileUpload;
