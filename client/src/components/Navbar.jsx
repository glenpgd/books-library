import { Link } from 'react-router-dom';

function Navbar() {
  return (
    <>
      <nav className='bg-gray-800 text-white p-4'>
        <div className='container mx-auto flex justify-between items-center'>
          <div className='flex gap-4'>
            <Link to='/' className='hover:text-gray-300'>
              Home
            </Link>
            <Link to='/add' className='hover:text-gray-300'>
              Add Book
            </Link>
          </div>
        </div>
      </nav>
    </>
  );
}
export default Navbar;
