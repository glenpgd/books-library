import PropTypes from 'prop-types'; // Import PropTypes
import { Link } from 'react-router-dom';

function BooksCard({ id, cover, desc, title, price, handleDelete }) {
  console.log(cover);
  return (
    <div
      key={id}
      className='flex flex-col items-center gap-2.5 w-full sm:w-1/2 md:w-1/3 lg:w-1/4'
    >
      <Link to={'/books/' + id}>
        <img
          src={`http://localhost:8800/images/${cover}`}
          alt='Book cover'
          className='w-auto h-auto object-cover bg-emerald-100 hover:shadow-2xl'
        />
      </Link>

      <div>
        <h2 className='font-bold capitalize'>{title}</h2>
        <p>{desc}</p>
        <p>${price}</p>
        <div className='flex flex-col items-center gap-2'>
          <button
            onClick={() => handleDelete(id)}
            className='border border-gray-200 bg-white cursor-pointer px-1.5 py-0.5 text-indigo-300  hover:bg-indigo-300 hover:text-white'
          >
            delete
          </button>
          <button className='border border-gray-200 bg-white cursor-pointer px-1.5 py-0.5 text-red-300 hover:bg-red-300 hover:text-white'>
            <Link to={`update/${id}`}>update</Link>
          </button>
        </div>
      </div>
    </div>
  );
}

// Define PropTypes
BooksCard.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  cover: PropTypes.string,
  desc: PropTypes.string,
  title: PropTypes.string,
  price: PropTypes.number,
  handleDelete: PropTypes.func.isRequired,
};

export default BooksCard;
