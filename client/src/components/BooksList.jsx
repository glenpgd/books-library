import PropTypes from 'prop-types'; // Import PropTypes
import BooksCard from './BooksCard';
import Books from '../pages/Books';

function BooksList({ books, handleDelete }) {
  return (
    <>
      <div className='flex flex-wrap gap-10 justify-center'>
        {books.map((el) => (
          <BooksCard key={el.id} {...el} handleDelete={handleDelete} />
        ))}
      </div>
      <div className='flex flex-wrap gap-10 justify-center'>
        {books.map((el) => (
          <Books key={el.id} {...el} handleDelete={handleDelete} />
        ))}
      </div>
    </>
  );
}

// Define PropTypes
BooksList.propTypes = {
  books: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
      // Define other properties of book objects if necessary
    })
  ).isRequired,
  handleDelete: PropTypes.func.isRequired,
};

export default BooksList;
