import FileUpload from './FileUpload';

function SubmitForm({ handleChange, handleSubmit, text, handleFile }) {
  return (
    <div className='min-h-screen flex flex-col items-center justify-center bg-gray-50 py-12 px-4 sm:px-6 lg:px-8'>
      <h1 className='text-2xl font-bold text-gray-900 mb-6'>{text}</h1>

      <form className='max-w-md w-full space-y-8' onSubmit={handleSubmit}>
        <div>
          <label htmlFor='title' className='sr-only'>
            Title
          </label>
          <input
            type='text'
            name='title'
            placeholder='Title'
            onChange={handleChange}
            className='appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm'
          />
        </div>
        <div>
          <label htmlFor='desc' className='sr-only'>
            Description
          </label>
          <input
            type='text'
            name='desc'
            placeholder='Description'
            onChange={handleChange}
            className='appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm'
          />
        </div>
        <div>
          <label htmlFor='cover'>Cover URL</label>
          <input type='file' name='cover' onChange={handleFile} />
        </div>
        <div>
          <label htmlFor='price' className='sr-only'>
            Price
          </label>
          <input
            type='number'
            name='price'
            placeholder='Price'
            onChange={handleChange}
            className='appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm'
          />
        </div>
        <div>
          <button
            type='submit'
            className='group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'
          >
            Update Book
          </button>
        </div>
      </form>
    </div>
  );
}
export default SubmitForm;
