import express, { request, response } from 'express';
import mysql from 'mysql';
import cors from 'cors';
import multer from 'multer';
import path from 'path';
import dotenv from 'dotenv';

dotenv.config();

const app = express();
const db = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
});
const serverPort = process.env.SERVER_PORT || 3000;

app.use(express.json());
app.use(cors());
app.use(express.static('public'));

// Storage
const storage = multer.diskStorage({
  destination: (request, file, Fn) => {
    Fn(null, process.env.UPLOAD_PATH); // Correctly specifying the destination folder
  },
  filename: (request, file, Fn) => {
    Fn(
      null,
      file.fieldname + '_' + Date.now() + path.extname(file.originalname)
    );
  },
});
const upload = multer({
  storage: storage,
});

// API Calls
app.get('/books', (request, response) => {
  db.query('SELECT * FROM test.books', (err, data) => {
    if (err) return response.json(err);
    return response.json(data);
  });
});

app.post('/books', upload.single('cover'), (request, response) => {
  const mysqlQuery =
    'INSERT INTO books (`title`, `desc`, `cover`, `price`) VALUES (?, ?, ?, ?)';
  const values = [
    request.body.title,
    request.body.desc,
    request.file.filename,
    request.body.price,
  ];

  db.query(mysqlQuery, values, (err, data) => {
    if (err) return response.json(err);
    return response.json('Book has been created successfully');
  });
});

app.delete('/books/:id', (request, response) => {
  const bookId = request.params.id;
  const mysqlQuery = 'DELETE FROM books WHERE id = ?';

  db.query(mysqlQuery, [bookId], (err, data) => {
    if (err) return response.json(err);
    return response.json('Book has been deleted successfully');
  });
});

app.put('/books/:id', upload.single('cover'), (request, response) => {
  const bookId = request.params.id;
  const mysqlQuery =
    'UPDATE books SET `title` = ?, `desc` = ?, `cover` = ?, `price` = ?  WHERE id = ?';
  const values = [
    request.body.title,
    request.body.desc,
    request.file.filename,
    request.body.price,
  ];

  db.query(mysqlQuery, [...values, bookId], (err, data) => {
    if (err) return response.json(err);
    return response.json('Book has been updated successfully');
  });
});

// PORT LISTENING
app.listen(serverPort, () => {
  console.log('Server is connected!');
});
