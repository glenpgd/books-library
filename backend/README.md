// CRUD Operations

// 1. import libraries
import express, { request, response } from 'express';
import mysql from 'mysql';
import cors from 'cors';
// Multer is a node.js middleware for handling multipart/form-data,
// which is primarily used for uploading files
import multer from 'multer';
import path from 'path';
import dotenv from 'dotenv';

// Load environment variables
dotenv.config();

// 2. invoke library
const app = express();
const db = mysql.createConnection({
host: process.env.DB_HOST,
user: process.env.DB_USER,
password: process.env.DB_PASSWORD,
database: process.env.DB_DATABASE,
});
// If issues with connection, write this query into workbench:
// ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';

// Use SERVER_PORT environment variable, with a default value if it's not set
const serverPort = process.env.SERVER_PORT || 3000;

//5. So we can send requests from the client side
// This is usually the body json
app.use(express.json());

//6. Another middleware library so we can see the data in the front end
app.use(cors());

app.use(express.static('public'));

//Bonus: 7 -> The disk storage engine gives you full control on storing files to disk.
const storage = multer.diskStorage({
destination: (request, file, Fn) => {
Fn(null, process.env.UPLOAD*PATH); // Correctly specifying the destination folder
},
filename: (request, file, Fn) => {
Fn(
null,
file.fieldname + '*' + Date.now() + path.extname(file.originalname)
); // Correctly generating the filename
},
});

const upload = multer({
storage: storage,
});

//4. start using library

/_
get -> /
_/
app.get('/', (request, response) => {
db.query('SELECT \* FROM test.books', (err, data) => {
if (err) return response.json(err);
return response.json(data);
});
});

/_
get -> /
_/
app.get('/', (request, response) => {
response.json('This is an API request to the backend server');
});

/_
get -> /books
_/
app.get('/books', (request, response) => {
db.query('SELECT \* FROM test.books', (err, data) => {
if (err) return response.json(err);
return response.json(data);
});
});

/_
post -> /books
_/
app.post('/books', upload.single('cover'), (request, response) => {
const mysqlQuery =
'INSERT INTO books (`title`, `desc`, `cover`, `price`) VALUES (?, ?, ?, ?)';
const values = [
request.body.title,
request.body.desc,
request.file.filename,
request.body.price,
];

// Correct usage: Pass `values` directly without wrapping it in another array
db.query(mysqlQuery, values, (err, data) => {
if (err) return response.json(err);
return response.json('Book has been created successfully');
});
});

/_
delete -> /books
_/
app.delete('/books/:id', (request, response) => {
const bookId = request.params.id;
const mysqlQuery = 'DELETE FROM books WHERE id = ?';

db.query(mysqlQuery, [bookId], (err, data) => {
if (err) return response.json(err);
return response.json('Book has been deleted successfully');
});
});

/_
put -> /books
_/
app.put('/books/:id', upload.single('cover'), (request, response) => {
const bookId = request.params.id;
const mysqlQuery =
'UPDATE books SET `title` = ?, `desc` = ?, `cover` = ?, `price` = ? WHERE id = ?';
const values = [
request.body.title,
request.body.desc,
request.file.filename,
request.body.price,
];

db.query(mysqlQuery, [...values, bookId], (err, data) => {
if (err) return response.json(err);
return response.json('Book has been updated successfully');
});
});

// 3. use library (express) for connection
app.listen(serverPort, () => {
console.log('Server is connected!');
});
